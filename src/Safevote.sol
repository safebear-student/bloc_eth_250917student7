pragma solidity ^0.4.11;

contract Safevote {

    mapping (bytes32 => uint8) public votesRecieved;
    bytes32[] public candidateList;
    
    function Safevote(bytes32[] candidateNames) {
        candidateList = candidateNames;
    }

    function voteForCandidate(bytes32 candidate) {
        require (validCandidate(candidate) == true);
        votesRecieved[candidate] += 1;
    }

    function validCandidate(bytes32 candidate) returns (bool) {
        for (uint i = 0; i < candidateList.length; i++) {
            if (candidateList[i] == candidate) {
                return true;
            }
        }

        return false;
    }
}
